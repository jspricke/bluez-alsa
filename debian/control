Source: bluez-alsa
Section: utils
Priority: optional
Maintainer: Debian Bluetooth Maintainers <team+pkg-bluetooth@tracker.debian.org>
Uploaders: Nobuhiro Iwamatsu <iwamatsu@debian.org>
Build-Depends: debhelper-compat (= 13),
	dh-exec,
	libasound2-dev,
	libbluetooth-dev (>= 5.0),
	libdbus-1-dev (>= 1.6),
	libglib2.0-dev (>= 2.30),
	libsbc-dev (>= 1.2),
	libldacbt-enc-dev (>= 2.0.0),
	libldacbt-abr-dev (>= 2.0.0),
	libopenaptx-dev (>= 0.2.0),
	python3-docutils,
	pkgconf,
	systemd-dev
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://github.com/Arkq/bluez-alsa
Vcs-Git: https://salsa.debian.org/bluetooth-team/bluez-alsa.git
Vcs-Browser: https://salsa.debian.org/bluetooth-team/bluez-alsa

Package: libasound2-plugin-bluez
Architecture: linux-any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Section: libs
Description: Bluetooth Audio ALSA Backend (plugins)
 Bluetooth Audio ALSA Backend allow bluetooth audio without PulseAudio.
 .
 This project is a rebirth of a direct integration between Bluez and ALSA.
 Since Bluez >= 5, the build-in integration has been removed in favor of 3rd
 party audio applications. From now on, Bluez acts as a middleware between an
 audio application, which implements Bluetooth audio profile, and a Bluetooth
 audio device.
 .
 This package contains bluez plugin for ALSA plugin.

Package: bluez-alsa-utils
Architecture: linux-any
Depends: ${shlibs:Depends}, ${misc:Depends}, libasound2-plugin-bluez
Description: Bluetooth Audio ALSA Backend (utils)
 Bluetooth Audio ALSA Backend allow bluetooth audio without PulseAudio.
 .
 This project is a rebirth of a direct integration between Bluez and ALSA.
 Since Bluez >= 5, the build-in integration has been removed in favor of 3rd
 party audio applications. From now on, Bluez acts as a middleware between an
 audio application, which implements Bluetooth audio profile, and a Bluetooth
 audio device.
 .
 This package contains files that is tool using bluez-alsa plugin.
